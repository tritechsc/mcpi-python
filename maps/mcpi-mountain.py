# block-list-0123456789ABCDEF.py
# Documentation https://github.com/raspberrypilearning/getting-started-with-minecraft-pi/blob/master/worksheet.md
from mcpi.minecraft import Minecraft
from mcpi import block
from time import sleep

def init():
    #ip = "10.183.3.20"
    ip = "127.0.0.1"
    mc = Minecraft.create(ip, 4711)
    #mc.setting("world_immutable",True)
    return mc

def get_matrix():
	# color list
	hdl = open("mountain.txt", 'r')
	thelines = hdl.readlines()
	hdl.close()
	return thelines


def clearAir(mc,x,y,z):
  # 256×256×128
  x,y,z = 0,0,0
  #mc.setBlocks(x-128-2,y-63,z-128+2,x+127-1,y+127,z+128-1,0)
  #mc.setBlocks(x-128+10,y-1,z-64+10,x+127-10,y+127,z+63+10,0)
  mc.setBlocks(-128,-1,-128,128,64,128,0)
  

def check(mc,x,y,z):
	mc.setBlocks(x,y-10,z,x,y+2,z,35,15)
	mc.setBlocks(x+5,y-10,z+5,x+5,y+2,z+5,35,14)# RED   5,5
	mc.setBlocks(x-5,y-10,z+5,x-5,y+2,z+5,35,5) # GREEN -5,5 
	mc.setBlocks(x-5,y-10,z-5,x-5,y+2,z-5,35,3) # LIGHT BLUE -5, -5
	mc.setBlocks(x+5,y-10,z-5,x+5,y+2,z-5,35,4) # YELLOW 5, -5      
	
	mc.setBlocks(x+35,y-10,z+35,x+35,y+2,z+35,35,14)# RED   5,5
	mc.setBlocks(x-35,y-10,z+35,x-35,y+2,z+35,35,5) # GREEN -5,5 
	mc.setBlocks(x-35,y-10,z-35,x-35,y+2,z-35,35,3) # LIGHT BLUE -5, -5
	mc.setBlocks(x+35,y-10,z-35,x+35,y+2,z-35,35,4) # YELLOW 5, -5      

def matrix(mc,x,y,z,str_list):
  z1 = -50
  for h in range (0,len(str_list)-1):
    x1 = -50
    for l in range (0,len(str_list[h])-1):
      #print(str_list[h][l],end="") # debug
      #print(h,k," ",end="")
      theBlock = str_list[h][l]
      height = int(theBlock)
      print(x1,z1," - ",theBlock," height ",height," eol ",end="")
      #mc.setBlock(-1*x1,y,-1*z1,35,theBlock)
      mc.setBlocks(-1*x1,y,-1*z1,-1*x1,y+(height*3),-1*z1,1)
      #p = input("  p  ")
      x1 = x1 + 1
    z1 = z1 + 1
    
def snow(mc,x,y,z,str_list):
  z1 = -50
  for h in range (0,len(str_list)-1):
    x1 = -50
    for l in range (0,len(str_list[h])-1):
      #print(str_list[h][l],end="") # debug
      #print(h,k," ",end="")
      theBlock = str_list[h][l]
      height = int(theBlock)
      print(x1,z1," - ",theBlock," height ",height," eol ",end="")
      #mc.setBlock(-1*x1,y,-1*z1,35,theBlock)
      SNOW,ICE = 78,79
      mc.setBlock(-1*x1,y+(height*3)+1,-1*z1,SNOW)
      #p = input("  p  ")
      x1 = x1 + 1
    z1 = z1 + 1
    
    
def main():
  mc = init()
  str_list = get_matrix()
  print(str_list)
  r = len(str_list)
  c = len(str_list[0])
  print(r,c)
  mc.player.setPos(0,0,0)
  x,y,z = mc.player.getPos()
  print(x,y,z)
  clearAir(mc,x,y,z)
  mc.player.setPos(0,50,5)
  check(mc,0,0,0  )
  matrix(mc,x,y,z+5,str_list)
  snow(mc,x,y,z+5,str_list)
  #mc.player.setPos(x,y,z-5)
  mc.postToChat("0 20 0")
  mc.player.setPos(0,50,5)
  
if __name__ == "__main__":
  main()



'''
def get_matrix():
	# color list
	hdl = open("data.txt", 'r')
	thelines = hdl.readlines()
	hdl.close()
	return thelines


def main():
	str_list = []
	str_list = matrix()
	print(str_list)
	r = len(str_list)
	c = len(str_list[0])
	print(r,c)

	
if __name__ == '__main__':
	main()

wool 35,0  WHITE
wool 35,1  ORANGE
wool 35,2  DARK PINK 
wool 35,3  LIGHT BLUE 
wool 35,4  YELLOW
wool 35,5  GREEN
wool 35,6  LIGHT PINK
wool 35,7  DARK GRAY
wool 35,8  LIGHT GRAY
wool 35,9  TEAL OR DARKER BLUE 
wool 35,10 VIOLET
wool 35,11  DARK BLUE
wool 35,12  BROWN
wool 35,13  DARK GREEN
wool 35,14  RED
wool 35,15  BLACK

#API Blocks
#====================
#   AIR                   0
#   STONE                 1
#   GRASS                 2
#   DIRT                  3
#   COBBLESTONE           4
#   WOOD_PLANKS           5
#   SAPLING               6
#   BEDROCK               7
#   WATER_FLOWING         8
#   WATER                 8
#   WATER_STATIONARY      9
#   LAVA_FLOWING         10
#   LAVA                 10
#   LAVA_STATIONARY      11
#   SAND                 12
#   GRAVEL               13
#   GOLD_ORE             14
#   IRON_ORE             15
#   COAL_ORE             16
#   WOOD                 17
#   LEAVES               18
#   GLASS                20
#   LAPIS_LAZULI_ORE     21
#   LAPIS_LAZULI_BLOCK   22
#   SANDSTONE            24
#   BED                  26
#   COBWEB               30
#   GRASS_TALL           31
#   WOOL                 35
#   FLOWER_YELLOW        37
#   FLOWER_CYAN          38
#   MUSHROOM_BROWN       39
#   MUSHROOM_RED         40
#   GOLD_BLOCK           41
#   IRON_BLOCK           42
#   STONE_SLAB_DOUBLE    43
#   STONE_SLAB           44
#   BRICK_BLOCK          45
#   TNT                  46
#   BOOKSHELF            47
#   MOSS_STONE           48
#   OBSIDIAN             49
#   TORCH                50
#   FIRE                 51
#   STAIRS_WOOD          53
#   CHEST                54
#   DIAMOND_ORE          56
#   DIAMOND_BLOCK        57
#   CRAFTING_TABLE       58
#   FARMLAND             60
#   FURNACE_INACTIVE     61
#   FURNACE_ACTIVE       62
#   DOOR_WOOD            64
#   LADDER               65
#   STAIRS_COBBLESTONE   67
#   DOOR_IRON            71
#   REDSTONE_ORE         73
#   SNOW                 78
#   ICE                  79
#   SNOW_BLOCK           80
#   CACTUS               81
#   CLAY                 82
#   SUGAR_CANE           83
#   FENCE                85
#   GLOWSTONE_BLOCK      89
#   BEDROCK_INVISIBLE    95
#   STONE_BRICK          98
#   GLASS_PANE          102
#   MELON               103
#   FENCE_GATE          107
#   GLOWING_OBSIDIAN    246
#   NETHER_REACTOR_CORE 247
'''



