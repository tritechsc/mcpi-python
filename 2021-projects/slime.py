# Base project format.
# Documentation https://github.com/raspberrypilearning/getting-started-with-minecraft-pi/blob/master/worksheet.md
from mcpi.minecraft import Minecraft
from mcpi import block

ip = "127.0.0.1"
mc = Minecraft.create(ip, 4711)
mc.player.setPos(0,0,0)
mc.setBlocks(-19,-1,-19,30,64,19,0)
mc.setBlocks(-128,-3,-128,128,64,128,0)
mc.setBlocks(10,10,10,20,20,20,1)
mc.setBlock(1,1,2,35,15)
mc.setBlock(2,1,2,35,15)
mc.setBlock(3,1,2,35,15)
mc.setBlock(4,1,2,35,15)
mc.setBlock(5,1,2,35,15)
mc.setBlock(6,1,2,35,15)
mc.setBlock(7,1,2,35,15)
mc.setBlock(8,2,2,35,15)
mc.setBlock(9,2,2,35,15)
mc.setBlock(10,3,2,35,15)
mc.setBlock(0,2,2,35,15)
mc.setBlock(-1,2,2,35,15)
mc.setBlock(-2,3,2,35,15)
mc.setBlock(11,4,2,35,15)
mc.setBlock(11,5,2,35,15)
mc.setBlock(11,6,2,35,15)
mc.setBlock(11,7,2,35,15)
mc.setBlock(-3,4,2,35,15)
mc.setBlock(-3,5,2,35,15)
mc.setBlock(-3,6,2,35,15)
mc.setBlock(-3,7,2,35,15)
mc.setBlock(10,8,2,35,15)
mc.setBlock(9,9,2,35,15)
mc.setBlock(8,10,2,35,15)
mc.setBlock(7,10,2,35,15)
mc.setBlock(6,11,2,35,15)
mc.setBlock(5,12,2,35,15)
mc.setBlock(5,13,2,35,15)
mc.setBlock(5,14,2,35,15)
mc.setBlock(-2,8,2,35,15)
mc.setBlock(-1,9,2,35,15)
mc.setBlock(0,10,2,35,15)
mc.setBlock(1,10,2,35,15)
mc.setBlock(2,11,2,35,15)
mc.setBlock(3,12,2,35,15)
mc.setBlock(3,13,2,35,15)
mc.setBlock(3,14,2,35,15)
mc.setBlock(4,15,2,35,15)
mc.setBlock(7,4,2,35,14)
mc.setBlock(6,3,2,35,14)
mc.setBlock(5,3,2,35,14)
mc.setBlock(4,3,2,35,14)
mc.setBlock(3,3,2,35,14)
mc.setBlock(2,3,2,35,14)
mc.setBlock(1,4,2,35,14)
mc.setBlock(6,5,2,35,0)
mc.setBlock(7,6,2,35,0)
mc.setBlock(6,7,2,35,0)
mc.setBlock(5,6,2,35,0)
mc.setBlock(6,6,2,35,15)
mc.setBlock(2,5,2,35,0)
mc.setBlock(3,6,2,35,0)
mc.setBlock(2,7,2,35,0)
mc.setBlock(1,6,2,35,0)
mc.setBlock(2,6,2,35,15)
mc.setBlock(1,2,2,35,11)
mc.setBlock(2,2,2,35,11)
mc.setBlock(3,2,2,35,11)
mc.setBlock(4,2,2,35,11)
mc.setBlock(5,2,2,35,11)
mc.setBlock(6,2,2,35,11)
mc.setBlock(7,2,2,35,11)
mc.setBlock(7,3,2,35,11)
mc.setBlock(8,3,2,35,11)
mc.setBlock(9,3,2,35,11)
mc.setBlock(0,3,2,35,11)
mc.setBlock(1,3,2,35,11)
mc.setBlock(-1,3,2,35,11)
mc.setBlock(10,4,2,35,11)
mc.setBlock(9,4,2,35,11)
mc.setBlock(8,4,2,35,11)
mc.setBlock(6,4,2,35,11)
mc.setBlock(5,4,2,35,11)
mc.setBlock(4,4,2,35,11)
mc.setBlock(3,4,2,35,11)
mc.setBlock(2,4,2,35,11)
mc.setBlock(0,4,2,35,11)
mc.setBlock(-1,4,2,35,11)
mc.setBlock(-2,4,2,35,11)
mc.setBlock(10,5,2,35,11)
mc.setBlock(9,5,2,35,11)
mc.setBlock(8,5,2,35,11)
mc.setBlock(7,5,2,35,11)
mc.setBlock(5,5,2,35,11)
mc.setBlock(4,5,2,35,11)
mc.setBlock(3,5,2,35,11)
mc.setBlock(1,5,2,35,11)
mc.setBlock(0,5,2,35,11)
mc.setBlock(-1,5,2,35,11)
mc.setBlock(-2,5,2,35,11)
mc.setBlock(10,6,2,35,11)
mc.setBlock(9,6,2,35,11)
mc.setBlock(8,6,2,35,11)
mc.setBlock(4,6,2,35,11)
mc.setBlock(0,6,2,35,11)
mc.setBlock(-1,6,2,35,11)
mc.setBlock(-2,6,2,35,11)
mc.setBlock(10,7,2,35,11)
mc.setBlock(9,7,2,35,11)
mc.setBlock(8,7,2,35,11)
mc.setBlock(7,7,2,35,11)
mc.setBlock(5,7,2,35,11)
mc.setBlock(4,7,2,35,11)
mc.setBlock(3,7,2,35,11)
mc.setBlock(1,7,2,35,11)
mc.setBlock(0,7,2,35,11)
mc.setBlock(-1,7,2,35,11)
mc.setBlock(-2,7,2,35,11)
mc.setBlock(9,8,2,35,11)
mc.setBlock(8,8,2,35,11)
mc.setBlock(7,8,2,35,11)
mc.setBlock(6,8,2,35,11)
mc.setBlock(5,8,2,35,11)
mc.setBlock(4,8,2,35,11)
mc.setBlock(3,8,2,35,11)
mc.setBlock(2,8,2,35,11)
mc.setBlock(1,8,2,35,11)
mc.setBlock(0,8,2,35,11)
mc.setBlock(-1,8,2,35,11)
mc.setBlock(8,9,2,35,11)
mc.setBlock(7,9,2,35,11)
mc.setBlock(6,9,2,35,11)
mc.setBlock(5,9,2,35,11)
mc.setBlock(4,9,2,35,11)
mc.setBlock(3,9,2,35,11)
mc.setBlock(2,9,2,35,11)
mc.setBlock(1,9,2,35,11)
mc.setBlock(0,9,2,35,11)
mc.setBlock(6,10,2,35,11)
mc.setBlock(5,10,2,35,11)
mc.setBlock(4,10,2,35,11)
mc.setBlock(3,10,2,35,11)
mc.setBlock(2,10,2,35,11)
mc.setBlock(5,11,2,35,11)
mc.setBlock(4,11,2,35,11)
mc.setBlock(3,11,2,35,11)
mc.setBlock(4,12,2,35,11)
mc.setBlock(4,13,2,35,11)
mc.setBlock(4,14,2,35,11)


'''
#API Blocks
#====================
#   AIR                   0
#   STONE                 1
#   GRASS                 2
#   DIRT                  3
#   COBBLESTONE           4
#   WOOD_PLANKS           5
#   SAPLING               6
#   BEDROCK               7
#   WATER_FLOWING         8
#   WATER                 8
#   WATER_STATIONARY      9
#   LAVA_FLOWING         10
#   LAVA                 10
#   LAVA_STATIONARY      11
#   SAND                 12
#   GRAVEL               13
#   GOLD_ORE             14
#   IRON_ORE             15
#   COAL_ORE             16
#   WOOD                 17
#   LEAVES               18
#   GLASS                20
#   LAPIS_LAZULI_ORE     21
#   LAPIS_LAZULI_BLOCK   22
#   SANDSTONE            24
#   BED                  26
#   COBWEB               30
#   GRASS_TALL           31
#   WOOL                 35
#   FLOWER_YELLOW        37
#   FLOWER_CYAN          38
#   MUSHROOM_BROWN       39
#   MUSHROOM_RED         40
#   GOLD_BLOCK           41
#   IRON_BLOCK           42
#   STONE_SLAB_DOUBLE    43
#   STONE_SLAB           44
#   BRICK_BLOCK          45
#   TNT                  46
#   BOOKSHELF            47
#   MOSS_STONE           48
#   OBSIDIAN             49
#   TORCH                50
#   FIRE                 51
#   STAIRS_WOOD          53
#   CHEST                54
#   DIAMOND_ORE          56
#   DIAMOND_BLOCK        57
#   CRAFTING_TABLE       58
#   FARMLAND             60
#   FURNACE_INACTIVE     61
#   FURNACE_ACTIVE       62
#   DOOR_WOOD            64
#   LADDER               65
#   STAIRS_COBBLESTONE   67
#   DOOR_IRON            71
#   REDSTONE_ORE         73
#   SNOW                 78
#   ICE                  79
#   SNOW_BLOCK           80
#   CACTUS               81
#   CLAY                 82
#   SUGAR_CANE           83
#   FENCE                85
#   GLOWSTONE_BLOCK      89
#   BEDROCK_INVISIBLE    95
#   STONE_BRICK          98
#   GLASS_PANE          102
#   MELON               103
#   FENCE_GATE          107
#   GLOWING_OBSIDIAN    246
#   NETHER_REACTOR_CORE 247
'''



