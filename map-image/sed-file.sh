#!/bin/bash
sed -i "s/#FFFFFF/G/g" rainier.txt
sed -i "s/#ceb7b4/6/g" rainier.txt
sed -i "s/#dc898b/5/g" rainier.txt
sed -i "s/#ee8575/4/g" rainier.txt
sed -i "s/#d38260/3/g" rainier.txt
sed -i "s/#f3a776/2/g" rainier.txt
sed -i "s/#f0c274/1/g" rainier.txt
sed -i "s/#efde76/0/g" rainier.txt
sed -i "s/#-/7/g" rainier.txt


#ceb7b4
#dc898b
#ee8575
#d38260
#f3a776
#f0c274
#efde76
